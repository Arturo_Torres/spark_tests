This is a repository which contains a demo script to deploy SparkR functionality 
for NA cell filling and stl decomposition of time series by cell for PROBA-V 
satellite imagery from the European Space Agency (ESA). Futhermore, functions for 
pixel-based satellite imagery analysis within spacetime objects are provided. 

This demo is a simple workflow example to how to deploy scalable code for High 
Performance Computing (HPC) by using Apache Spark and R for this task. This
allows also to develop further workflows for big data processing and analysis.
